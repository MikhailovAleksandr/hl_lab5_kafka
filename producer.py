import time
from datetime import datetime
from kafka import KafkaProducer


producer = KafkaProducer(bootstrap_servers=["localhost:9092"])

if __name__ == "__main__":
    for i in range(1000):
        producer.send("lab5", str.encode(str(datetime.now())))
        time.sleep(0.5)
